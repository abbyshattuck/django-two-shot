from django.contrib import admin
from receipts.models import ExpenseCategory, Receipt, Account

admin.site.register(ExpenseCategory)


class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "owner",
    ]

    def __str__(self):
        return self.name


admin.site.register(Receipt)


class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    ]


admin.site.register(Account)


class AccountAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
        "owner",
    ]
