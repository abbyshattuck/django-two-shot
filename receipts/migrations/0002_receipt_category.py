# Generated by Django 4.2.7 on 2023-11-01 19:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="receipt",
            name="category",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="receipts",
                to="receipts.expensecategory",
            ),
        ),
    ]
